<?php

namespace Empu\Directory\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Empu\Directory\Models\Group;
use Empu\Directory\Models\Party;
use October\Rain\Database\Builder;
use October\Rain\Exception\ApplicationException;
use Session;

/**
 * Distinct Contacts Backend Controller
 */
class DistinctContacts extends Controller
{
    public $implement = [
        \Backend\Behaviors\FormController::class,
        \Backend\Behaviors\ListController::class,
    ];

    /**
     * @var string formConfig file
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string listConfig file
     */
    public $listConfig = 'config_list.yaml';

    public $activeType;

    /**
     * __construct the controller
     */
    public function __construct()
    {
        parent::__construct();

        $this->prepareActiveGroup();

        $sideMenuItemCode = sprintf('distinctcontacts_%s', $this->activeType->code);
        BackendMenu::setContext('Empu.Directory', 'directory', $sideMenuItemCode);

        if ($this->activeType->has_restriction) {
            $this->requiredPermissions = ["empu.directory.access_contacts.{$this->activeType->code}"];
        }
    }

    protected function prepareActiveGroup()
    {
        if ($typeCode = input('type')) {
            $type = $this->findGroup($typeCode);
            $this->setActiveGroup($type);
        } else {
            $this->activeType = $this->getActiveGroup();
        }
    }

    protected function findGroup($typeCode): Group
    {
        return Group::applyCode($typeCode)->firstOrFail();
    }

    /**
     * Returns an active search type for this widget instance.
     */
    public function getActiveGroup(): Group
    {
        $typeCode = Session::get('distinctcontacts_type');

        return $this->findGroup($typeCode);
    }

    /**
     * Sets an active search type for this widget instance.
     */
    public function setActiveGroup(?Group $type = null): void
    {
        if ($type instanceof Group) {
            Session::put('distinctcontacts_type', $type->code);
        } else {
            Session::forget('distinctcontacts_type');
        }

        $this->activeType = $type;
    }

    public function listExtendQuery(Builder $builder): void
    {
        if (!$this->activeType) {
            throw new ApplicationException('Contacts type not found!');
        }

        $builder->whereHas('groups', function ($query) {
            $query->applyCode($this->activeType->code);
        });
    }

    public function formExtendModel(Party $model)
    {
        if ($model->groups()->count() === 0) {
            $model->groups = [$this->activeType];
        }
    }
}
