<?php

namespace Empu\Directory\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreatePartyGroupsTable Migration
 */
class CreatePartiesGroupsTable extends Migration
{
    public function up()
    {
        Schema::create('empu_directory_parties_groups', function (Blueprint $table) {
            $table->unsignedBigInteger('party_id');
            $table->foreign('party_id', 'parties_groups_fk1')
                ->references('id')
                ->on('empu_directory_parties')
                ->onDelete('cascade');
            $table->unsignedTinyInteger('group_id');
            $table->foreign('group_id', 'parties_groups_fk2')
                ->references('id')
                ->on('empu_directory_groups')
                ->onDelete('cascade');
            $table->primary(['party_id', 'group_id'], 'parties_groups_pk');
        });
    }

    public function down()
    {
        Schema::dropIfExists('empu_directory_parties_groups');
    }
}
