<?php

namespace Empu\Directory\Models;

use Empu\EloquentSubtype\Contracts\InheritableEntity;
use October\Rain\Database\Builder;
use October\Rain\Database\Model;

/**
 * Party Model
 */
class Party extends Model implements InheritableEntity
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    /**
     * @var string table associated with the model
     */
    public $table = 'empu_directory_parties';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [
        'name', 'is_organization', 'phone', 'email',
        'address_line_1', 'address_line_2', 'city', 'state', 'country', 'zip_code',
    ];

    /**
     * @var array rules for validation
     */
    public $rules = [
        'name' => 'required',
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @var array hasOne and other relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'groups' => [
            Group::class,
            'pivotModel' => PivotPartyGroup::class,
            'table' => 'empu_directory_parties_groups',
            'key' => 'party_id',
            'otherKey' => 'group_id',
        ],
        'members' => [
            Party::class,
            'pivotModel' => OrganizationMember::class,
            'table' => 'empu_directory_organization_members',
            'key' => 'organization_id',
            'otherKey' => 'member_id',
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $descendible = [
        'id', 'name', 'is_organization', 'phone', 'email',
        'address_line_1', 'address_line_2', 'zip_code',
        'city', 'state', 'country',
    ];

    public function scopeAccessible(Builder $builder, array $includes = []): Builder
    {
        return $builder->where(function (Builder $q) use ($includes) {
            $q->whereHas('groups', fn(Builder $q) => $q->hasNoRestrictions($includes))
                ->orWhereDoesntHave('groups');
        });
    }

    public function scopePersonOnly(Builder $builder): Builder
    {
        return $builder->where($this->qualifyColumn('is_organization'), false);
    }

    public function scopeOrganizationOnly(Builder $builder): Builder
    {
        return $builder->where($this->qualifyColumn('is_organization'), true);
    }

    public function scopeApplyEmail(Builder $builder, string $email): Builder
    {
        return $builder->where($this->qualifyColumn('email'), $email);
    }

    public function attachTo(string $code): void
    {
        $group = Group::findByCode($code);
        $this->groups()->attach($group);
    }

    public function getIsOrganizationOptions(): array
    {
        return [
            '0' => 'Person',
            '1' => 'Organization',
        ];
    }

    public function descendibleColumns(): array
    {
        return $this->descendible;
    }

    public function qualifiedDescendibleColumns(): array
    {
        return collect($this->descendibleColumns())
            ->transform(fn ($column) => $this->qualifyColumn($column))
            ->toArray();
    }
}
