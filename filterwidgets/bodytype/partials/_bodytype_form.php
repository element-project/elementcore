<div class="filter-box">
    <div class="filter-facet">
        <div class="facet-item is-grow">
            <select name="Filter[value]" class="form-control form-control-sm custom-select select-no-search">
                <?php foreach ($scope->options as $key => $label): ?>
                    <option value="<?= $key ?>" <?= $scope->value == $key ? 'selected="selected"' : '' ?>><?= $label ?></option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <div class="filter-buttons">
        <button class="btn btn-sm btn-primary" data-filter-action="apply">
            <?= __("Apply") ?>
        </button>
        <div class="flex-grow-1"></div>
        <button class="btn btn-sm btn-secondary" data-filter-action="clear">
            <?= __("Clear") ?>
        </button>
    </div>
</div>
