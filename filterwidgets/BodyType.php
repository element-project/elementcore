<?php namespace Empu\Directory\FilterWidgets;

use Backend\Classes\FilterWidgetBase;
use Lang;

/**
 * BodyType Filter Widget
 */
class BodyType extends FilterWidgetBase
{
    const ALL = -1;
    const PERSON = 0;
    const ORGANIZATION = 1;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->filterScope->options(self::availableOptions());
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('bodytype');
    }

    /**
     * @inheritDoc
     */
    public function renderForm()
    {
        $this->prepareVars();
        return $this->makePartial('bodytype_form');
    }

    /**
     * prepareVars for view data
     */
    public function prepareVars()
    {
        $this->vars['scope'] = $this->filterScope;
        $this->vars['name'] = $this->getScopeName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['selectedOption'] = self::selectionOption($this->getLoadValue());
    }

    public static function selectionOption(?array $value = null): ?string
    {
        $selected = array_first($value);

        if (is_null($selected)) {
            return null;
        }

        return self::availableOptions()[$selected];
    }

    public static function availableOptions(): array
    {
        $options = [
            self::ALL => Lang::get('empu.directory::lang.party.all'),
            self::PERSON => Lang::get('empu.directory::lang.party.person'),
            self::ORGANIZATION => Lang::get('empu.directory::lang.party.organization'),
        ];

        return $options;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        // $this->addCss('css/bodytype.css', 'Empu.Directory');
        // $this->addJs('js/bodytype.js', 'Empu.Directory');
    }

    /**
     * getActiveValue
     */
    public function getActiveValue()
    {
        if (post('clearScope')) {
            return null;
        }

        if (!$this->hasPostValue('value')) {
            return null;
        }

        return post('Filter');
    }

    /**
     * applyScopeToQuery
     */
    public function applyScopeToQuery($query)
    {
        $value = intval($this->filterScope->value);

        if ($value === self::PERSON) {
            $query->where('is_organization', false);
        }
        elseif ($value === self::ORGANIZATION) {
            $query->where('is_organization', true);
        }
    }
}
